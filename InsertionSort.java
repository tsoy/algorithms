import java.lang.System;
import java.util.Arrays;
public class InsertionSort {
    public static void main(String[] args){
        System.out.println("Diana");
        InsertionSort insertion = new InsertionSort();
        int[] a = {5,4,2,1};
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(insertion.sort(a)));

    }

    public int[] sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int j = i - 1;
            int tmp = arr[i];
            while (j >= 0 && arr[j] > tmp) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = tmp;
        }
        return arr;
    }
}